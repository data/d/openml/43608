# OpenML dataset: Personality-traits-of-Twitter-users-(celebrities)

https://www.openml.org/d/43608

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Finding similarities between different types of public figures based in their Twitter activity.
.
Content
    usuario - username
    op = Openness to experience
    co =Conscientiousness
    ex = Extraversion
    ag = Agreeableness
    ne = Neuroticism
    Wordcount- word count lol
    Categoria  user's laboral activity(actor, singer, etc.)
Acknowledgements
Inspiration

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43608) of an [OpenML dataset](https://www.openml.org/d/43608). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43608/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43608/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43608/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

